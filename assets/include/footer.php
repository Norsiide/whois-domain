    <footer class="footer">
      <div class="container">
        <div class="row row-grid align-items-center mb-5">
          <div class="col-lg-6">
            <h3 class="text-primary font-weight-light mb-2">Whois Domaine!</h3>
            <h4 class="mb-0 font-weight-light">Information sur un domaine</h4>
          </div>
          <div class="col-lg-6 text-lg-center btn-wrapper">
            <button target="_blank" href="https://twitter.com/norsiide" rel="nofollow" class="btn btn-icon-only btn-twitter rounded-circle" data-toggle="tooltip" data-original-title="Follow us">
              <span class="btn-inner--icon"><i class="fab fa-twitter"></i></span>
            </button>
            <button target="_blank" href="https://www.twitch.tv/norsiide" rel="nofollow" class="btn-icon-only rounded-circle btn btn-twitch" style="background-color:#609" data-toggle="tooltip" data-original-title="Like us">
              <span class="btn-inner--icon"><i class="fab fa-twitch" style="color:white"></i></span>
            </button>
            <button target="_blank" href="https://clouding-host.com/discord" rel="nofollow" class="btn btn-icon-only btn-default rounded-circle" style="background-color:#7289da" data-toggle="tooltip" data-original-title="Follow us">
              <span class="btn-inner--icon"><i class="fab fa-discord" style="color:white"></i></span>
            </button>
          </div>
        </div>
        <hr>
        <div class="row align-items-center justify-content-md-between">
          <div class="col-md-6">
            <div class="copyright">
              &copy; 2020 <a href="" target="_blank">By Norsiide</a>.
            </div>
          </div>
          <div class="col-md-6">
            <ul class="nav nav-footer justify-content-end">
              <li class="nav-item">
                <a href="https://twitter.com/norsiide" class="nav-link" target="_blank">Contacter moi</a>
              </li>
              <li class="nav-item">
                <a href="https://clouding-host.com/discord" class="nav-link" target="_blank">Rejoin moi sur discord</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
  </div>