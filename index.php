<?php

include('functions.php');

$result = '';

$domain= '';

$message = '';

if(isset($_POST['domain'])){ 

	$domain = $_POST['domain'];	

	$domain = trim($domain);

	if(substr(strtolower($domain), 0, 7) == "http://") $domain = substr($domain, 7);

	if(substr(strtolower($domain), 0, 8) == "https://") $domain = substr($domain, 8);

	if(substr(strtolower($domain), 0, 4) == "www.") $domain = substr($domain, 4);

	if(validateDomain($domain)) {

		$result = lookUpDomain($domain);

	} else {

		$message = "Erreur du domaine!";

	}

}

?>

<!DOCTYPE html>

  <html lang="fr">

<head>

  <title>Whois | Norsiide.be</title>

    <?php include('assets/include/head.php'); ?>

</head>

<body class="profile-page">

<div class="wrapper">

  <?php include('assets/include/navbar.php'); ?>

  <section class="section section-shaped section-lg">

    <div class="shape shape-style-1 bg-gradient-default">

      <span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span>

    </div>

    <div class="container pt-lg-7">

      <div class="row justify-content-center">

        <div class="col-lg-12">

          <div class="card bg-secondary shadow border-0">

            <div class="card-header bg-white pb-5">

              	<div class="text-muted text-center mb-3"><H1>WHOIS DOMAINE</H1></div>	

					<label class="text-info">

						<?php if($message) { ?>

							<span class="text-danger"><strong><?php echo $message; ?></strong></span>

						<?php } ?>	

					</label>																	

						<form name="form" class="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">

							<div class="input-group mb-3">

								<input type="text" name="domain" id="domain" class="form-control" value="<?php echo $domain; ?>" placeholder="<?php echo $_SERVER['SERVER_NAME']; ?>" required>

								<div class="input-group-append">

									<button class="btn btn-success"  type="submit"><i class="fa fa-search"></i> Rechercher</button>

								</div>

							</div>			

						</form>				

						<div class="card-body px-lg-5 py-lg-5">

								<?php if($result) { echo "<pre>\n" . $result . "\n</pre>\n"; } ?>

						</div>

					</div>

				</div>

            	<div class="row mt-3">

					<div class="col-6">

						<a href="https://clouding-host.com/web/domain" class="text-light"><small>Acheter un domaine?</small></a>

					</div>

            		<div class="col-6 text-right">

              			<a href="https://clouding-host.com/web/hosting" class="text-light"><small>Serveur web mutualisé?</small></a>

            		</div>

          		</div>

        	</div>

      	</div>

    </div>

</section>

<?php include("assets/include/footer.php"); ?>

<?php include('assets/include/scripts.php'); ?>

</body>

</html>





